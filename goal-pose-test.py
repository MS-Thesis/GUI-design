#!/usr/bin/env python
import Tkinter as tk
import tkMessageBox
import MySQLdb
import databaseconfig as cfg
import re

root = tk.Tk()
root.title("Goal pose")
root.iconbitmap('@icon_goal.xbm')
frame = tk.Frame(root, height = 700, width = 850)
frame.pack()

## Define variables

v1 = tk.IntVar()
buttonval1 = tk.IntVar()
buttonval2 = tk.IntVar()
dialog = tkMessageBox
layer_var = tk.StringVar(frame)
layer_var.set("1")
part_var = tk.StringVar(frame)
part_var.set("1")

try:
    connecttodb = MySQLdb.connect(cfg.mysql['host'], cfg.mysql['user'], cfg.mysql['passwd'], cfg.mysql['database'])
except MySQLdb.Error:
    print"Connection error"

cursor = connecttodb.cursor()

## Define callbacks
def extract_pallet():
	pallet = v1.get()
	lay = layer_var.get()
	part = part_var.get()

	if pallet == 1:
		pallet = "left-pallet-%s-%s" % (lay,part)
	elif pallet == 2:
		pallet = "right-pallet-%s-%s" % (lay,part)
	elif pallet == 0:
		pallet = "none selected"
	return pallet


def extract_pose(pose_name):
	cursor.execute("SELECT toolpose from urtest where poselabel = '%s'" % pose_name)
	current_pose = cursor.fetchall()
	return current_pose

def extract_int(ip_string):
	## regular expression to extract floating point numbers from string
	return map(float, re.findall(r"[-+]?\d*\.\d+|\d+",ip_string))

def compute_change(pose_name):
	pose_now = extract_pose(pose_name)
	pose_now_int = extract_int("%s" % pose_now)
	change_pose = [None]*6
	change_pose[0] = pose_now_int[0] + X.get()
	change_pose[1] = pose_now_int[1] + Y.get()
	change_pose[2] = pose_now_int[2] + Z.get()
	change_pose[3] = pose_now_int[3] + Roll.get()
	change_pose[4] = pose_now_int[4] + Pitch.get()
	change_pose[5] = pose_now_int[5] + Yaw.get()
	return change_pose

def update_pose_db(pose_new,which_side):
	query = ("UPDATE urtest SET toolpose ='%s' 	WHERE poselabel='%s'" % (pose_new,which_side))
	return query

def callback_submit():
	print("Submit")
	result = dialog.askyesno("Warning!","Check if the robot is collision free. Are you sure to submit commands?")
	if result == True:
		which_part = extract_pallet()
		print which_part
		print "current pose: %s" % (extract_pose(which_part))
		new_pose = compute_change(which_part)
		print "new pose: %s" % (new_pose)
		cursor.execute(update_pose_db(new_pose,which_part))
		connecttodb.commit()
	pass

def callback_reset():
	print("Reset")
	X.set(0)
	Y.set(0)
	Z.set(0)
	Roll.set(0)
	Pitch.set(0)
	Yaw.set(0)
	v1.set(0)
	pass

def lang_change():
	label1.config(text = "CIELO`VA' POZI'CIA")
	label2.config(text = "Paleta Stranu")
	label3.config(text = "C`islo vrstvy :")
	label4.config(text = "Poc`et dielov :")
	button1.config(text = "L'ava' Paleta") 
	button2.config(text = "Prava' Paleta")
	button3.config(text = "Potvrdit'")
	button4.config(text = "Resetovat'")
	Roll.config(label ="Rota'cia -X (rad)")
	Pitch.config(label = "Rota'cia -Y (rad)")
	Yaw.config(label = "Rota'cia -Z (rad)")
	pass
def lang_change1():
	label1.config(text = "GOAL POSE (World frame):")
	label2.config(text = "Choose pallet side:")
	label3.config(text = "Layer Number :")
	label4.config(text = "Part Number :")
	button1.config(text = "Left Pallet") 
	button2.config(text = "Right Pallet")
	button3.config(text = "Submit")
	button4.config(text = "Reset")
	Roll.config(label = "Roll-RX (rad)")
	Pitch.config(label = "Pitch-RY (rad)")
	Yaw.config(label = "Yaw-RZ (rad)")
	pass


logo = tk.PhotoImage(file="logo.png")

w1 = tk.Label(frame, image=logo)
w1.pack()
w1.place(x=0, y=0)

logo_unige = tk.PhotoImage(file="unige_logo.png")

w2 = tk.Label(frame, image=logo_unige)
w2.pack()
w2.place(x=450, y=0)

logo_robot = tk.PhotoImage(file="rob.png")

w3 = tk.Label(frame, image=logo_robot)
w3.pack()
w3.place(x=700, y=200)

logo_pallet = tk.PhotoImage(file="pallet_new.png")

w3 = tk.Label(frame, image=logo_pallet)
w3.pack()
w3.place(x=690, y=450)

label1 = tk.Label(frame, 
        text="""GOAL POSE (World frame):""",
        justify = tk.CENTER,
        padx = 20, font=("Helvetica", 14))
label1.pack()
label1.place(x=190, y= 80)


label2 = tk.Label(frame, 
        text="""Choose pallet side:""",
        justify = tk.CENTER,
        padx = 20)
label2.pack()
label2.place(x=10, y= 120)



button1 = tk.Radiobutton(frame, 
              text="Left Pallet",
              padx = 20, 
              variable=v1, 
              value=1)
button1.pack()
button1.place(x=10, y= 140)

button2 = tk.Radiobutton(frame, 
              text="Right Pallet",
              padx = 20, 
              variable=v1, 
              value=2)
button2.pack()
button2.place(x=120, y= 140)

photo = tk.PhotoImage(file="slovak.png")
button_lang = tk.Button(frame, image = photo, command = lang_change)
button_lang.pack()
button_lang.place(x= 700, y = 100)

photo2 = tk.PhotoImage(file="england.png")
button_lang1 = tk.Button(frame, image = photo2, command = lang_change1)
button_lang1.pack()
button_lang1.place(x= 770, y = 100)


## Menu widgets

label3 = tk.Label(frame, 
        text="Layer Number : ",
        justify = tk.CENTER,
        padx = 20)
label3.pack()
label3.place(x=240, y= 140)

Layer = tk.OptionMenu(frame, layer_var,"1", "2", "3", "4","5")
Layer.pack()
Layer.place(x= 370, y= 132)



label4 = tk.Label(frame, 
        text="Part Number : ",
        justify = tk.CENTER,
        padx = 20)
label4.pack()
label4.place(x=420, y= 140)

Part_menu = tk.OptionMenu(frame, part_var,"1","6","7","12","13","18")
Part_menu.pack()
Part_menu.place(x= 550, y= 132)


## Scale widgets

X = tk.Scale(frame, from_=-10, to=10, label= "X (mm)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1)
X.set(0)
X.pack()
X.place(x= 25, y= 160)

Y = tk.Scale(frame, from_=-10, to=10, label= "Y (mm)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1)
Y.set(0)
Y.pack()
Y.place(x= 25, y= 240)

Z = tk.Scale(frame, from_=-10, to=10, label= "Z (mm)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1)
Z.set(0)
Z.pack()
Z.place(x= 25, y= 320)

Roll = tk.Scale(frame, from_=-10, to=10, label= "Roll-RX (rad)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1)
Roll.set(0)
Roll.pack()
Roll.place(x= 25, y= 400)

Pitch = tk.Scale(frame, from_=-10, to=10, label= "Pitch-RY (rad)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1)
Pitch.set(0)
Pitch.pack()
Pitch.place(x= 25, y= 480)

Yaw = tk.Scale(frame, from_=-10, to=10, label= "Yaw-RZ (rad)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1)
Yaw.set(0)
Yaw.pack()
Yaw.place(x= 25, y= 560)


button3 = tk.Button(frame, text="Submit", command=callback_submit)
button3.pack()
button3.place(x=400 , y= 650)

button4 = tk.Button(frame, text="Reset", command=callback_reset)
button4.pack()
button4.place(x = 550, y= 650)


root.mainloop()
