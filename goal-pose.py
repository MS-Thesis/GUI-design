import Tkinter as tk
import tkMessageBox
# from Tkinter import messagebox

root = tk.Tk()
root.title("Goal pose")
root.iconbitmap('@icon_goal.xbm')
frame = tk.Frame(root, height = 700, width = 850)
frame.pack()

## Define variables

v1 = tk.IntVar()
buttonval1 = tk.IntVar()
buttonval2 = tk.IntVar()
dialog = tkMessageBox
layer_var = tk.StringVar(frame)
layer_var.set("1")
part_var = tk.StringVar(frame)
part_var.set("1")
## Define callbacks

def callback_rb():
	print("radio_button: %d" % v1.get())
	pass
def callback_slx(val1):
	print("x-slider: %f" % X.get())
	pass
def callback_sly(val2):
	print("y-slider: %f" % Y.get())
	pass
def callback_slz(val3):
	print("z-slider: %f" % Z.get())
	pass
def callback_slroll(val4):
	print("roll-slider: %f" % Roll.get())
	pass
def callback_slpitch(val5):
	print("pitch-slider: %f" % Pitch.get())
	pass
def callback_slyaw(val6):
	print("yaw-slider: %f" % Yaw.get())
	pass
def callback_submit():
	print("Submit")
	dialog.askyesno("Warning!","Check if the robot is collision free. Are you sure to submit commands?")
	pass
def callback_reset():
	print("Reset")
	X.set(0)
	Y.set(0)
	Z.set(0)
	Roll.set(0)
	Pitch.set(0)
	Yaw.set(0)
	v1.set(0)
	pass

def lang_change():
	label1.config(text = "CIELO`VA' POZI'CIA")
	label2.config(text = "Paleta Stranu")
	label3.config(text = "C`islo vrstvy :")
	label4.config(text = "Poc`et dielov :")
	button1.config(text = "L'ava' Paleta") 
	button2.config(text = "Prava' Paleta")
	button3.config(text = "Potvrdit'")
	button4.config(text = "Resetovat'")
	Roll.config(label ="Rota'cia -X (rad)")
	Pitch.config(label = "Rota'cia -Y (rad)")
	Yaw.config(label = "Rota'cia -Z (rad)")
	pass
def lang_change1():
	label1.config(text = "GOAL POSE (World frame):")
	label2.config(text = "Choose pallet side:")
	label3.config(text = "Layer Number :")
	label4.config(text = "Part Number :")
	button1.config(text = "Left Pallet") 
	button2.config(text = "Right Pallet")
	button3.config(text = "Submit")
	button4.config(text = "Reset")
	Roll.config(label = "Roll-RX (rad)")
	Pitch.config(label = "Pitch-RY (rad)")
	Yaw.config(label = "Yaw-RZ (rad)")
	pass

def callback_menu2(frame):
	print (part_var.get())
	pass

logo = tk.PhotoImage(file="logo.png")

w1 = tk.Label(frame, image=logo)
w1.pack()
w1.place(x=0, y=0)

logo_unige = tk.PhotoImage(file="unige_logo.png")

w2 = tk.Label(frame, image=logo_unige)
w2.pack()
w2.place(x=450, y=0)

logo_robot = tk.PhotoImage(file="rob.png")

w3 = tk.Label(frame, image=logo_robot)
w3.pack()
w3.place(x=700, y=300)

label1 = tk.Label(frame, 
        text="""GOAL POSE (World frame):""",
        justify = tk.CENTER,
        padx = 20, font=("Helvetica", 14))
label1.pack()
label1.place(x=190, y= 80)


label2 = tk.Label(frame, 
        text="""Choose pallet side:""",
        justify = tk.CENTER,
        padx = 20)
label2.pack()
label2.place(x=10, y= 120)



button1 = tk.Radiobutton(frame, 
              text="Left Pallet",
              padx = 20, 
              variable=v1, 
              value=1, command=callback_rb)
button1.pack()
button1.place(x=10, y= 140)

button2 = tk.Radiobutton(frame, 
              text="Right Pallet",
              padx = 20, 
              variable=v1, 
              value=2, command=callback_rb)
button2.pack()
button2.place(x=120, y= 140)

photo = tk.PhotoImage(file="slovak.png")
button_lang = tk.Button(frame, image = photo, command = lang_change)
button_lang.pack()
button_lang.place(x= 700, y = 100)

photo2 = tk.PhotoImage(file="england.png")
button_lang1 = tk.Button(frame, image = photo2, command = lang_change1)
button_lang1.pack()
button_lang1.place(x= 770, y = 100)


## Menu widgets

label3 = tk.Label(frame, 
        text="Layer Number : ",
        justify = tk.CENTER,
        padx = 20)
label3.pack()
label3.place(x=240, y= 140)

Layer = tk.OptionMenu(frame, layer_var,"1", "2", "3", "4","5")
Layer.pack()
Layer.place(x= 370, y= 132)



label4 = tk.Label(frame, 
        text="Part Number : ",
        justify = tk.CENTER,
        padx = 20)
label4.pack()
label4.place(x=420, y= 140)

Part_menu = tk.OptionMenu(frame, part_var,"1", "2", "3", "4","5","6","7","8","9","10","11","12","13","14","15","16","17","18",command= callback_menu2)
Part_menu.pack()
Part_menu.place(x= 550, y= 132)


## Scale widgets

X = tk.Scale(frame, from_=-10, to=10, label= "X (mm)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1,  command = callback_slx)
X.set(0)
X.pack()
X.place(x= 25, y= 160)

Y = tk.Scale(frame, from_=-10, to=10, label= "Y (mm)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1,  command = callback_sly)
Y.set(0)
Y.pack()
Y.place(x= 25, y= 240)

Z = tk.Scale(frame, from_=-10, to=10, label= "Z (mm)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1,  command = callback_slz)
Z.set(0)
Z.pack()
Z.place(x= 25, y= 320)

Roll = tk.Scale(frame, from_=-10, to=10, label= "Roll-RX (rad)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1,  command = callback_slroll)
Roll.set(0)
Roll.pack()
Roll.place(x= 25, y= 400)

Pitch = tk.Scale(frame, from_=-10, to=10, label= "Pitch-RY (rad)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1,  command = callback_slpitch)
Pitch.set(0)
Pitch.pack()
Pitch.place(x= 25, y= 480)

Yaw = tk.Scale(frame, from_=-10, to=10, label= "Yaw-RZ (rad)", length=650, tickinterval=1, orient= tk.HORIZONTAL, resolution= 0.1,  command = callback_slyaw)
Yaw.set(0)
Yaw.pack()
Yaw.place(x= 25, y= 560)


button3 = tk.Button(frame, text="Submit", command=callback_submit)
button3.pack()
button3.place(x=400 , y= 650)

button4 = tk.Button(frame, text="Reset", command=callback_reset)
button4.pack()
button4.place(x = 550, y= 650)


root.mainloop()
